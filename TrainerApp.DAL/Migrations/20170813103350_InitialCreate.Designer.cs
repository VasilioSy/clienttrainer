﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using TrainerApp.DAL;

namespace TrainerApp.DAL.Migrations
{
    [DbContext(typeof(TrainerAppDbContext))]
    [Migration("20170813103350_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("TrainerApp.DAL.Entities.Exercise", b =>
                {
                    b.Property<int>("ExerciseId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<int>("ExerciseTypeId");

                    b.Property<string>("Name");

                    b.Property<string>("Url");

                    b.HasKey("ExerciseId");

                    b.HasIndex("ExerciseTypeId");

                    b.ToTable("Exercises");
                });

            modelBuilder.Entity("TrainerApp.DAL.Entities.ExerciseType", b =>
                {
                    b.Property<int>("ExerciseTypeId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("ExerciseTypeId");

                    b.ToTable("ExerciseTypes");
                });

            modelBuilder.Entity("TrainerApp.DAL.Entities.Exercise", b =>
                {
                    b.HasOne("TrainerApp.DAL.Entities.ExerciseType", "ExerciseType")
                        .WithMany()
                        .HasForeignKey("ExerciseTypeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
