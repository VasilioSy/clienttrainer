﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrainerApp.DAL.Entities
{
    public class Set
    {
        public int SetId { get; set; }
        public int Repetitions { get; set; }
        public int Weight { get; set; }

    }
}
