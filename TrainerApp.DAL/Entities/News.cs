﻿namespace TrainerApp.DAL.Entities
{
    public class News
    {
        public int NewsId { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
    }
}
