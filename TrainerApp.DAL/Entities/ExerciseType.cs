﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrainerApp.DAL.Entities
{
    public class ExerciseType
    {
        public int ExerciseTypeId { get; set; }
        public string Name { get; set; }
    }
}
