﻿namespace TrainerApp.DAL.Entities
{
    public class UserStatus
    {
        public int UserStatusId { get; set; }
        public string StatusName { get; set; }
    }
}
