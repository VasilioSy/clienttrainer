﻿using Microsoft.EntityFrameworkCore;
using TrainerApp.DAL.Entities;

namespace TrainerApp.DAL
{
    public class TrainerAppDbContext : DbContext
    {
        public TrainerAppDbContext(DbContextOptions<TrainerAppDbContext> options)
            : base(options)
        { }

        public DbSet<Exercise> Exercises { get; set; }
        public DbSet<ExerciseType> ExerciseTypes { get; set; }
    }
}
